from textwrap import wrap, dedent
import re, sys, os, shutil, os.path
from .services import *
from . import storage
# from lesson_utils import (
#     user_input, multi_line_input, input_datetime, input_int
# )



def analiz_data(analiz=None):

    """Запрашивает от функций данные анализа"""
    analiz = dict(analiz) if analiz else {}
    data = {}

    data['name'] = get_name()

    data['AT_GC_lenght'] = primary_analysis()

    data['quantity_gen'] = gen_search()

    """Добавляет анализ в БД"""
    with make_connection() as conn:
        #data=analiz_data()
        analiz= storage.create_analiz(conn, **data)
        print('данные занесены в БД')

def all():
    """Вывводит все данные"""

    with make_connection() as conn:
        analiz = storage.get_all_analiz(conn)
        print("id | name | AT_GC, П.О. | генов найдено")
        for a in analiz:
            print(f"{p['id']} |{p['name']}| {p['AT_GC_lenght']} | {p['quantity_gen']} ")

def primary_analysis():
    len_seq=(len(seq))

    dct = {}
    for i in seq:
        if i in dct:
            dct[i] += 1
        else:
            dct[i] = 1
    A=dct.get('a')
    G=dct.get('g')
    C=dct.get('c')
    T=dct.get('t')
    AT=round(((A+T*100)/len_seq),3)
    GC=round((100-AT),3)
    print(f"длина проследовательности {len_seq} П.Н. \n AT/GC % : {AT}/{GC}")
    return AT, GC, len_seq


def entry_search ():
    desired = str(input("Введите искомую последовательность: "))
    if seq.find(desired) != -1:
        print ('Yes')
    else:
        print('No')

def create_mrna():
    mrna_dictionary= str.maketrans("catg", "guac")
    mrna=str.translate(seq, mrna_dictionary)
    mrna_file = open('mrna.txt', 'w')
    mrna_file.write(mrna)
    mrna_file.close()
    print("фаил с м-РНК успешно создан")
    shutil.move(r"mrna.txt", way)


def translation():

    translation_matrix = {
        'uuu':'phe', 'uuc':'phe', 'ucu':'ser', 'ucc':'ser',
        'uau':'tyr', 'uac':'tyr', 'ugu':'cys', 'ugc':'cys',
        'uua':'leu', 'uca':'ser', 'uug':'leu', 'ucg':'ser',
        'ugg':'trp', 'cuu':'leu', 'cuc':'leu', 'ccu':'pro',
        'cau':'his', 'cac':'his', 'cgu':'arg', 'cgc':'arg',
        'cua':'leu', 'cug':'leu', 'cca':'pro', 'ccg':'pro',
        'caa':'gln', 'cag':'gln', 'cga':'arg', 'cgg':'arg',
        'auu':'ile', 'auc':'ile', 'acu':'thr', 'acc':'thr',
        'aau':'asn', 'aac':'asn', 'agu':'ser', 'agc':'ser',
        'aua':'ile', 'aca':'thr', 'aaa':'lys', 'aga':'arg',
        'aug':'met', 'acg':'thr', 'aag':'lys', 'agg':'arg',
        'guu':'val', 'guc':'val', 'gcu':'ala', 'gcc':'ala',
        'gau':'asp', 'gac':'asp', 'ggu':'gly', 'ggc':'gly',
        'gua':'val', 'gug':'val', 'gca':'ala', 'gcg':'ala',
        'gaa':'glu', 'gag':'glu', 'gga':'gly', 'ggg':'gly',
        'ccc':'pro'}


    mrna_dictionary= str.maketrans("catg", "guac")
    mrna=str.translate(seq, mrna_dictionary)
    mrna=wrap(mrna, 3)

    peptid=[]
    for i in mrna :
        peptid.append(translation_matrix.get(i))
    peptid_string = str(peptid)

    peptid_file = open('peptid_file.txt', 'w')
    peptid_file.write(peptid_string)
    peptid_file.close()
    print("фаил с аминокислотной последовательностью успешно создан")
    shutil.move(r"peptid_file.txt", way)

def gen_search():
    pattern=r'(tac(.*?)att)|(tac(.*?)atc)|(tac(.*?)act)'
    gen=re.split(pattern, seq)
    gen = [x for x in gen if x]

    gen_out=[]
    for i in range(len(gen)):
        if i % 2:
            gen_out.append(gen[i])

    quantity_gen=len(gen_out)

    gen_string=str(gen_out)

    gen_file = open('gen_file.txt', 'w')
    gen_file.write(gen_string)
    gen_file.close()
    print(f"фаил успешно создан. генов найдено: {quantity_gen}")
    shutil.move(r"gen_file.txt", os.path.join(way, "gen_file.txt"))
    return quantity_gen

def cloze():
    """Закрыть программу"""
    sys.exit(0)


def read_read():
    global seq
    global way
    global filename
    filename = input("Введите путь к риду: ")
    seq = open(filename, 'r+')
    seq = seq.read()
    way=os.path.dirname(filename)

def get_name():
    name=os.path.basename(filename)
    return name


def menu():
    print(dedent('''
    1. Анализировать новую последовательность
    2. Открыть базу данных
    q. Закрыть программу
    '''))
    while 1:
        d={
        '1': submenu,
        '2': all,
        'q': cloze
        }
        i=input ("Bведите команду: ")
        if i in d:
            d[i]()
        else: print('Bведите коректно')

def submenu():
    read_read()

    while 1:
        print(dedent('''
        1. Первичный анализ
        2. Поиск вхождения
        3. Создать м-РНК
        4. Трансляция
        5. Поиск генов
        6. Внести данные в БД
        m. Открыть основное меню
        '''))
        d={
        '1': primary_analysis,
        '2': entry_search,
        '3': create_mrna,
        '4': translation,
        '5': gen_search,
        '6': analiz_data,
        'm': menu
        }
        i=input ("Bведите команду: ")
        if i in d:
            d[i]()
        else: print('Bведите коректно')


def main():
    with make_connection() as conn:
        schema_path = os.path.join(os.path.join(os.path.dirname(__file__)), "resourses\\schema.sql") #pkg_resources.resource_filename(__name__, 'resources/schema.sql')
        #print(schema_path)
        storage.initialize(conn, schema_path)

    menu()


# read_read()
# get_name()
if __name__ == '__main__':
    main()
