SQL_CREATE_NEW_ANALIZ = '''
    INSERT INTO dna_analyzes (name, AT_GC_lenght, quantity_gen)
    VALUES (?, ?, ?)
'''
SQL_SELECT_ALL_ANALIZ = '''
    SELECT
        *
    FROM
        dna_analyzes
'''

def initialize(conn, creation_schema):
    """Инициализирует структуру базы данных"""
    with open(creation_schema) as f:
        conn.executescript(f.read())

def create_analiz(conn, name, AT_GC_lenght, quantity_gen):
    """Сохраняет новый анализ в БД и возвращает его."""
    cursor = conn.execute(SQL_CREATE_NEW_ANALIZ, (name, AT_GC_lenght, quantity_gen))
    pk = cursor.lastrowid # последний сгенерированный первичный ключ
    conn.commit()
    return get_analiz(conn, pk)

def get_all_analiz(conn):
    cursor = conn.execute(SQL_SELECT_ALL_ANALIZ)
    return cursor.fetchall()
