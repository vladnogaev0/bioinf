from configparser import ConfigParser
import sqlite3, os
#from lesson_utils.app_utils import get_config_dir, get_data_dir

def make_config(*config_files):
    config = ConfigParser()
    config.read(config_files)
    return config


config = make_config('C:/Users/Vlad/Documents/bio-inf/bio_inf/resourses/config.ini')

def make_connection(name='db'):
    """Возвращает объект-подключения к БД SQLite"""
    db_name = config.get(name, 'db_name')

    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    conn.row_factory = sqlite3.Row

    return conn
