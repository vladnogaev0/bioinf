from configparser import ConfigParser
import sqlite3, os

def make_config(*config_files):
    config = ConfigParser()
    config.read(config_files)
    return config


config = make_config(os.path.join(os.path.join(os.path.dirname(__file__)), 'resourses/config.ini'))

def make_connection(name='db'):
    """Возвращает объект-подключения к БД SQLite"""
    db_name = config.get(name, 'db_name')

    conn = sqlite3.connect(db_name, detect_types=sqlite3.PARSE_DECLTYPES)
    conn.row_factory = sqlite3.Row

    return conn
