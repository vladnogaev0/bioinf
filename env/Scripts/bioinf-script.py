#!c:\users\vlad\documents\bio-inf\env\scripts\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'bio-inf','console_scripts','bioinf'
__requires__ = 'bio-inf'
import re
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(
        load_entry_point('bio-inf', 'console_scripts', 'bioinf')()
    )
