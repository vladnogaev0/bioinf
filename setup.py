from setuptools import setup, find_packages


setup(
    name='bio-inf',
    version='0.0.1',
    description='Сonsole bioinformatics application.',
    license='Apache License 2.0',
    author='Vlad Nogaev',
    author_email='v.nogaev@mail.ru',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'bioinf = bio_inf:main',
        ],
    }
)
